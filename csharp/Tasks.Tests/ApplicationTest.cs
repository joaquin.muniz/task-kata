using System;
using System.IO;
using NUnit.Framework;

namespace Tasks
{
	[TestFixture]
	public sealed class ApplicationTest
	{
		public const string PROMPT = "> ";

		private FakeConsole console;
		private System.Threading.Thread applicationThread;

		[SetUp]
		public void StartTheApplication()
		{
			this.console = new FakeConsole();
			var taskList = new TaskList(console);
			this.applicationThread = new System.Threading.Thread(() => taskList.Run());
			applicationThread.Start();
		}

		[TearDown]
		public void KillTheApplication()
		{
			if (applicationThread == null || !applicationThread.IsAlive)
			{
				return;
			}
			applicationThread.Abort();
			throw new Exception("The application is still running.");
		}
		
		[Test, Timeout(1000)]
		public void ShowEmptyProject()
		{
			Execute("add project secrets");
			Execute("show");
			ReadLines("secrets", "");
			Execute("quit");
		}
		
		[Test, Timeout(1000)]
		public void AddTasks()
		{
			Execute("add project secrets");
			
			Execute("add task secrets Eat more donuts.");
			Execute("add task secrets Destroy all humans.");

			Execute("show");
			ReadLines(
				"secrets",
				"    [ ] 1: Eat more donuts.",
				"    [ ] 2: Destroy all humans.",
				""
			);
			Execute("quit");
		}
		
		[Test, Timeout(1000)]
		public void CheckTasks()
		{
			Execute("add project secrets");
			
			Execute("add task secrets Eat more donuts.");
			Execute("add task secrets Destroy all humans.");
			Execute("check 1");
			
			Execute("show");
			ReadLines(
				"secrets",
				"    [x] 1: Eat more donuts.",
				"    [ ] 2: Destroy all humans.",
				""
			);
			Execute("quit");
		}
		
		[Test, Timeout(1000)]
		public void AddDeadline()
		{
			Execute("add project secrets");
			
			Execute("add task secrets Eat more donuts.");
			Execute("add task secrets Destroy all humans.");
			Execute("deadline 1 2021-01-12");
			
			Execute("show");
			ReadLines(
				"secrets",
				"    [ ] 1: Eat more donuts. - Deadline 2021-01-12",
				"    [ ] 2: Destroy all humans.",
				""
			);
			Execute("quit");
		}

		private void Execute(string command)
		{
			Read(PROMPT);
			Write(command);
		}

		private void Read(string expectedOutput)
		{
			var length = expectedOutput.Length;
			var actualOutput = console.RetrieveOutput(expectedOutput.Length);
			Assert.AreEqual(expectedOutput, actualOutput);
		}
		
		private void ReadLines(params string[] expectedOutput)
		{
			foreach (var line in expectedOutput)
			{
				Read(line + Environment.NewLine);
			}
		}

		private void Write(string input)
		{
			console.SendInput(input + Environment.NewLine);
		}
	}
}
