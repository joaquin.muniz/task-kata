﻿using System.Collections.Generic;
using Tasks.Commands;

namespace Tasks
{
	public sealed class TaskList
	{
		private const string QUIT = "quit";

		private readonly IDictionary<string, IList<Task>> tasks = new Dictionary<string, IList<Task>>();
		private readonly IDictionary<string, ICommand> commands = new Dictionary<string, ICommand>();
		private readonly IConsole console;

		public static void Main(string[] args)
		{
			new TaskList(new RealConsole()).Run();
		}

		public TaskList(IConsole console)
		{
			this.console = console;
			AddCommand(new ShowCommand());
			AddCommand(new AddCommand());
			AddCommand(new CheckCommand());
			AddCommand(new UncheckCommand());
			AddCommand(new DeadlineCommand());
			AddCommand(new HelpCommand());
		}

		void AddCommand(ICommand command)
		{
			command.SetUp(tasks);
			commands.Add(command.Name, command);
		}

		public void Run()
		{
			while (true) {
				console.Write("> ");
				var command = console.ReadLine();
				if (command == QUIT) {
					break;
				}
				Execute(command);
			}
		}

		private void Execute(string commandLine)
		{
			var commandRest = commandLine.Split(" ".ToCharArray(), 2);
			var commandName = commandRest[0];
			if (commands.TryGetValue(commandName, out ICommand value))
			{
				var parameters = commandRest.Length > 1 ? commandRest[1] : ""; 
				console.Write(value.Execute(parameters));
				return;
			}
			Error(commandName);
		}

		private void Error(string command)
		{
			console.WriteLine("I don't know what the command \"{0}\" is.", command);
		}
	}
}
