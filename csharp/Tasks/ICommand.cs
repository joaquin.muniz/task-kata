using System.Collections.Generic;

namespace Tasks
{
    public interface ICommand
    {
        string Name { get; }
        void SetUp(IDictionary<string, IList<Task>> tasks);
        string Execute(string parameters);
    }
}