using System.Collections.Generic;
using System.Text;

namespace Tasks.Commands
{
    public class HelpCommand : ICommand
    {
        public string Name => "help";
        public void SetUp(IDictionary<string, IList<Task>> tasks)
        {
            
        }

        public string Execute(string parameters)
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.AppendLine("Commands:");
            strBuilder.AppendLine("  show");
            strBuilder.AppendLine("  add project <project name>");
            strBuilder.AppendLine("  add task <project name> <task description>");
            strBuilder.AppendLine("  check <task ID>");
            strBuilder.AppendLine("  uncheck <task ID>");
            strBuilder.AppendLine();
            return strBuilder.ToString();
        }
    }
}