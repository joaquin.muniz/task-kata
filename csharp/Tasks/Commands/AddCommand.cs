using System.Collections.Generic;
using System.Text;

namespace Tasks.Commands
{
    public class AddCommand : ICommand
    {
        public string Name => "add";
        private IDictionary<string, IList<Task>> tasks;
        private StringBuilder strBuilder;
        private int lastId = 0;
        
        public void SetUp(IDictionary<string, IList<Task>> tasks)
        {
            this.tasks = tasks;
        }

        public string Execute(string parameters)
        {
            strBuilder = new StringBuilder();
            var subcommandRest = parameters.Split(" ".ToCharArray(), 2);
            var subcommand = subcommandRest[0];
            if (subcommand == "project") {
                AddProject(subcommandRest[1]);
            } else if (subcommand == "task") {
                var projectTask = subcommandRest[1].Split(" ".ToCharArray(), 2);
                AddTask(projectTask[0], projectTask[1]);
            }

            return strBuilder.ToString();
        }
        
        private void AddProject(string name)
        {
            tasks[name] = new List<Task>();
        }
        
        private void AddTask(string project, string description)
        {
            if (!tasks.TryGetValue(project, out IList<Task> projectTasks))
            {
                strBuilder.AppendLine($"Could not find project {project}");
                return;
            }
            projectTasks.Add(new Task { Id = NextId(), Description = description, Done = false });
        }

        private int NextId()
        {
            return ++lastId;
        }
    }
}