using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Tasks.Commands
{
    public class DeadlineCommand : ICommand
    {
        public string Name => "deadline";
        private IDictionary<string, IList<Task>> tasks;
        public void SetUp(IDictionary<string, IList<Task>> tasks)
        {
            this.tasks = tasks;
        }

        public string Execute(string parameters)
        {
            StringBuilder strBuilder = new StringBuilder();
            var subcommandRest = parameters.Split(" ".ToCharArray(), 2);
            var subcommand = subcommandRest[0];
			
            if (int.TryParse(subcommand, out int id)) {
				
                Task identifiedTask = tasks
                    .Select(project => project.Value.FirstOrDefault(task => task.Id == id))
                    .FirstOrDefault(task => task != null);

                if (identifiedTask != null)
                {
                    identifiedTask.Deadline = DateTime.Parse(subcommandRest[1]);
                }

            } else if (subcommand == "task") {
                strBuilder.AppendLine($"Could not find a task with an ID of {id}.");
            }

            return strBuilder.ToString();
        }
    }
}