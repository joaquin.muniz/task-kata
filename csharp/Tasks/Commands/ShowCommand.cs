using System.Collections.Generic;
using System.Text;

namespace Tasks.Commands
{
    public class ShowCommand : ICommand
    {
        public string Name => "show";
        private IDictionary<string, IList<Task>> tasks;
        
        public void SetUp(IDictionary<string, IList<Task>> tasks)
        {
            this.tasks = tasks;
        }

        public string Execute(string parameters)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var project in tasks) {
                stringBuilder.AppendLine(project.Key);
                foreach (var task in project.Value)
                {
                    var deadline = (task.Deadline.HasValue ? $" - Deadline {task.Deadline.Value:yyyy-MM-dd}" : "");
                    stringBuilder.AppendLine($"    [{(task.Done ? 'x' : ' ')}] {task.Id}: {task.Description}{deadline}");
                }
                stringBuilder.AppendLine("");
            }

            return stringBuilder.ToString();
        }
    }
}