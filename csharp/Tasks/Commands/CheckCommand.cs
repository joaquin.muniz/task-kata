using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tasks.Commands
{
    public class CheckCommand : ICommand
    {
        public string Name => "check";
        private IDictionary<string, IList<Task>> tasks;
        
        public void SetUp(IDictionary<string, IList<Task>> tasks)
        {
            this.tasks = tasks;
        }

        public string Execute(string parameters)
        {
            StringBuilder strBuilder = new StringBuilder();
            int id = int.Parse(parameters);
            var identifiedTask = tasks
                .Select(project => project.Value.FirstOrDefault(task => task.Id == id))
                .FirstOrDefault(task => task != null);
            if (identifiedTask == null) {
                strBuilder.AppendLine($"Could not find a task with an ID of {id}.");
                return strBuilder.ToString();
            }

            identifiedTask.Done = true;
            return strBuilder.ToString();
        }
    }
}